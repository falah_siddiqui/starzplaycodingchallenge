CREATE TABLE `payment_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `display_name` varchar(45) DEFAULT NULL,
  `payment_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;


CREATE TABLE `payment_plans` (
  `id` int(11) NOT NULL,
  `net_amount` double DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `gross_amount` double DEFAULT NULL,
  `currency` varchar(45) DEFAULT NULL,
  `duration` varchar(45) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`payment_method_id`),
  CONSTRAINT `id` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
