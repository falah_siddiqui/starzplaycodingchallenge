#Payment Methods API

A Spring boot restful services to provide payment methods that are available for the customer to pay for video platform service.
##Prerequisites

This application requires:
 * JDK 8 or later
 * Maven 3.5 or later
 
##Assumptions
 * Multiple PaymentMethods with same name can exists
 * The update query will remove the existing record and then add the updated one.
 * The filter on id is for paymentMethods and it returns the whole object for PaymentMethods

##Architecture
The application follows a layered architecture:
 * The **controller** receives the request and calls the respective end-point.
 * Controller calls the service layer which communicates to the Repository, manipulates the data and returns a reponse to the controller.
 * **Service layer** implements the business logic on the data received from the Repository Layer

##Explore APIs
The app defines the following APIs:

 * GET http://server_address/api/v1.0/configuration/payment-methods	(Fetches all the records for Payment Methods)
 * GET http://server_address/api/v1.0/configuration/payment-methods?name={name} (Filtered records on the basis of name)
 * GET http://server_address/api/v1.0/configuration/payment-methods?id={id}	(Filtered records on the basis of id)
 * POST http://server_address/api/v1.0/configuration/payment-methods (Adds a new PaymentMethods)
 * PUT http://server_address/api/v1.0/configuration/payment-methods?payment-methods={id} (Updates an exisiting Payment Method Record)