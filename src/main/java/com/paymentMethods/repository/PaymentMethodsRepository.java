package com.paymentMethods.repository;

import com.paymentMethods.model.PaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * This is a Repository interface which provides basic methods to perform operations with the DB
 * table payment_methods
 *
 * @author falah.siddiqui
 */
@Repository
public interface PaymentMethodsRepository
        extends JpaRepository<PaymentMethod, Integer>, JpaSpecificationExecutor<PaymentMethod> {

    /**
     * This method Returns all instances of type Payment methods
     */
    List<PaymentMethod> findAll();

    /**
     * This method finds te record having specified name
     *
     * @param name
     * @return
     */
    PaymentMethod findByName(String name);

    /**
     * This method deletes te record having specified name
     *
     * @param name
     */
    @Transactional
    void deleteByName(String name);
}
