package com.paymentMethods.service;

import com.paymentMethods.adapter.PaymentMethodsAdapter;
import com.paymentMethods.model.PaymentMethod;
import com.paymentMethods.model.PaymentMethodFilter;
import com.paymentMethods.model.PaymentMethodRequest;
import com.paymentMethods.model.PaymentMethodResponse;
import com.paymentMethods.repository.PaymentMethodsRepository;
import com.paymentMethods.specification.PaymentMethodSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Collections;

/**
 * This service class is an implementation of PaymentMethodsServiceInterface
 *
 * @author falah.siddiqui
 */
@Service
public class PaymentMethodsService implements PaymentMethodsServiceInterface {

    static final Integer PAGE = 0;
    static final Integer SIZE = 50;

    @Autowired
    private PaymentMethodsRepository paymentMethodsRepository;

    @Autowired
    private PaymentMethodsAdapter paymentMethodsAdapter;

    /**
     * {@link PaymentMethodsServiceInterface#getPaymentMethods(PaymentMethodFilter)}
     *
     * @param payMethodFilter
     * @return
     */
    @Override
    public PaymentMethodResponse getPaymentMethods(PaymentMethodFilter payMethodFilter) {

        Page<PaymentMethod> paymentMethodPage = null;
        if (!ObjectUtils.isEmpty(payMethodFilter)) {
            paymentMethodPage = paymentMethodsRepository
                    .findAll(PaymentMethodSpecification.findAll(payMethodFilter), PageRequest.of(PAGE, SIZE));
            return paymentMethodsAdapter.convertEntityToDomain(paymentMethodPage.getContent());
        }
        return paymentMethodsAdapter.convertEntityToDomain(paymentMethodsRepository.findAll());
    }

    /**
     * {@link PaymentMethodsServiceInterface#addPaymentMethod(PaymentMethodRequest)}
     *
     * @param paymentMethodRequest
     * @return
     * @throws Exception
     */
    @Override
    public PaymentMethodResponse addPaymentMethod(PaymentMethodRequest paymentMethodRequest) throws Exception {
        PaymentMethod paymentMethod = paymentMethodsAdapter.convertDomainToEntity(paymentMethodRequest);
        try {
            paymentMethodsRepository.save(paymentMethod);
        }
        catch (Exception e){
            throw new Exception("Couldn't Add the Record!");
        }

        PaymentMethodResponse response =
                new PaymentMethodResponse(Collections.singletonList(paymentMethod));
        return response;
    }

    /**
     * {@link PaymentMethodsServiceInterface#updatePaymentMethod(PaymentMethodRequest)}
     *
     * @param paymentMethodRequest
     * @return
     * @throws Exception
     */
    @Override
    public PaymentMethodResponse updatePaymentMethod(PaymentMethodRequest paymentMethodRequest) throws Exception {

        PaymentMethod paymentMethod = new PaymentMethod();
        PaymentMethod payMethod = new PaymentMethod();
        String name = paymentMethodRequest.getName();
        paymentMethod = paymentMethodsRepository.findByName(name);
        if (!ObjectUtils.isEmpty(paymentMethod)) {
            paymentMethodsRepository.deleteByName(name);
            payMethod = paymentMethodsAdapter.convertDomainToEntity(paymentMethodRequest);
            paymentMethodsRepository.save(payMethod);
        }
        else
            throw new Exception("Record does not exists");
        PaymentMethodResponse response =
                new PaymentMethodResponse(Collections.singletonList(payMethod));
        return response;
    }
}
