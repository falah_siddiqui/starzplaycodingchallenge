package com.paymentMethods.service;

import com.paymentMethods.model.PaymentMethod;
import com.paymentMethods.model.PaymentMethodFilter;
import com.paymentMethods.model.PaymentMethodRequest;
import com.paymentMethods.model.PaymentMethodResponse;

/**
 * 
 * 
 * @author falah.siddiqui
 *
 */
public interface PaymentMethodsServiceInterface {

  /**
   * This method fetches Payment Methods records and also filters them based on name or Payment Plan id
   *
   * @param payMethodFilter
   * @return
   */
  public PaymentMethodResponse getPaymentMethods(PaymentMethodFilter payMethodFilter);

  /**
   * This method is used to add a new payment mathod record
   *
   * @param paymentMethod
   * @return
   * @throws Exception
   */
  public PaymentMethodResponse addPaymentMethod(PaymentMethodRequest paymentMethod) throws Exception;

  /**
   * This method is used to update an existing payment mathod record
   *
   * @param paymentMethodRequest
   * @return
   * @throws Exception
   */
  public PaymentMethodResponse updatePaymentMethod(PaymentMethodRequest paymentMethodRequest) throws Exception;

}
