package com.paymentMethods.web;

import com.paymentMethods.model.ErrorMessage;
import com.paymentMethods.model.PaymentMethodFilter;
import com.paymentMethods.model.PaymentMethodRequest;
import com.paymentMethods.service.PaymentMethodsService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This controller class contains restful end-points for Payment Methods
 *
 * @author falah.siddiqui
 */

@RestController
@RequestMapping("/api/v1.0/configuration/payment-methods")
public class PaymentMethodsController {

    @Autowired
    PaymentMethodsService paymentMethodsService;

    /**
     * This end-point provides getMapping for all payment methods and also filters based on name or Payment Plan id
     *
     * @param payMethodFilter
     * @return
     */
    @GetMapping("")
    public ResponseEntity<?> getAllPaymentMethods(PaymentMethodFilter payMethodFilter) {
        return ResponseEntity.ok().body(paymentMethodsService.getPaymentMethods(payMethodFilter));
    }

    /**
     * This end-point provides postMapping for payment method records and is used to add records
     *
     * @param paymentMethod
     * @return
     * @throws Exception
     */
    @PostMapping("")
    public ResponseEntity<?> addPaymentMethod(@RequestBody @NonNull PaymentMethodRequest paymentMethod) throws Exception {
        try {
            return ResponseEntity.ok().body(paymentMethodsService.addPaymentMethod(paymentMethod));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ErrorMessage(e.getMessage()));
        }
    }

    /**
     * This end-point provides putMapping for paymentMethod records and is used to update records
     *
     * @param paymentMethod
     * @return
     * @throws Exception
     */
    @PutMapping("")
    public ResponseEntity<?> updatePaymentMethod(@RequestBody PaymentMethodRequest paymentMethod) throws Exception {
        try {
            return ResponseEntity.ok().body(paymentMethodsService.updatePaymentMethod(paymentMethod));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ErrorMessage(e.getMessage()));
        }
    }
}
