package com.paymentMethods.adapter;

import java.util.List;
import org.springframework.stereotype.Component;
import com.paymentMethods.model.PaymentMethod;
import com.paymentMethods.model.PaymentMethodRequest;
import com.paymentMethods.model.PaymentMethodResponse;
import com.paymentMethods.model.PaymentPlan;

/**
 * This class is an adapter to convert Domain into entities and vice-versa
 */
@Component
public class PaymentMethodsAdapter {

  /**
   * This method converts an Object of PaymentMethod to PaymentMethodResponse
   *
   * @param paymentMethod
   * @return
   */
  public PaymentMethodResponse convertEntityToDomain(List<PaymentMethod> paymentMethod) {

    PaymentMethodResponse methodResponse = new PaymentMethodResponse(paymentMethod);
    return methodResponse;

  }

  /**
   * This method converts an Object of PaymentMethodResponse to PaymentMethod
   *
   * @param paymentMethodRequest
   * @return
   */
  public PaymentMethod convertDomainToEntity(PaymentMethodRequest paymentMethodRequest) {
    PaymentMethod paymentMethod = new PaymentMethod(paymentMethodRequest.getName(),
        paymentMethodRequest.getDisplayName(), paymentMethodRequest.getPaymentType());

    paymentMethod.getPaymentPlans();
    paymentMethodRequest.getPaymentPlans().stream().forEach(p -> {
      PaymentPlan paymentPlan = new PaymentPlan(p.getNetAmount(), p.getTaxAmount(),
          p.getGrossAmount(), p.getCurrency(), p.getDuration(), paymentMethod);
      paymentMethod.addPaymentPlan(paymentPlan);
    });
    return paymentMethod;
  }

}
