package com.paymentMethods.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * POJO class for PaymentMethodResponse
 */
@AllArgsConstructor
@Data
public class PaymentMethodResponse {

  private List<PaymentMethod> paymentMethods;

}
