package com.paymentMethods.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Entity class for PaymentPlan
 * 
 * @author falah.siddiqui
 *
 */

@Entity
@Table(name = "payment_plans")
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class PaymentPlan implements Serializable{

  @GeneratedValue
  @Id
  private Integer id;
  private Double netAmount;
  private Double taxAmount;
  private Double grossAmount;
  private String currency;
  private String duration;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "payment_method_id")
  @JsonIgnore
  private PaymentMethod paymentMethod;

  public PaymentPlan(Double netAmount, Double taxAmount, Double grossAmount, String currency,
      String duration, PaymentMethod paymentMethod) {
    this.netAmount = netAmount;
    this.taxAmount = taxAmount;
    this.grossAmount = grossAmount;
    this.currency = currency;
    this.duration = duration;
    this.paymentMethod = paymentMethod;
  }

}
