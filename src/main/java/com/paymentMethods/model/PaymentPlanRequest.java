package com.paymentMethods.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * POJO class for PaymentPlanRequest
 */
@Data
@NoArgsConstructor
public class PaymentPlanRequest {
  private Double netAmount;
  private Double taxAmount;
  private Double grossAmount;
  private String currency;
  private String duration;
}
