package com.paymentMethods.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * This class contains error messages
 */
@Data
@AllArgsConstructor
public class ErrorMessage {
    private String message;
}
