package com.paymentMethods.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Entity class for PaymentMethod
 * 
 * @author falah.siddiqui
 *
 */

@Entity
@Table(name = "payment_methods")
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class PaymentMethod implements Serializable{

  @GeneratedValue
  @Id
  @JsonIgnore
  private Integer id;
  private String name;
  private String displayName;
  @Enumerated(EnumType.STRING)
  private PaymentType paymentType;
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "paymentMethod", cascade = CascadeType.ALL, orphanRemoval = true)
  List<PaymentPlan> paymentPlans;
  
  /**
   * @param name
   * @param disName
   * @param payType
   */
  public PaymentMethod(String name, String disName, PaymentType payType) {
    this.name = name;
    this.displayName = disName;
    this.paymentType = payType;
    this.paymentPlans = new ArrayList<>();
  }
  
  /**
   * @param paymentPlan
   */
  public void addPaymentPlan(PaymentPlan paymentPlan) {
    this.paymentPlans.add(paymentPlan);
  }

}
