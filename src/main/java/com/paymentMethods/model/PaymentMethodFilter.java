package com.paymentMethods.model;

import lombok.Data;

/**
 * POJO class for PaymentMethodFilter
 */
@Data
public class PaymentMethodFilter {

    private String name;
    private Integer id;
    private Integer pageNumber;
    private Integer pageSize;

}
