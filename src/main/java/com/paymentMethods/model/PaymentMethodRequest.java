package com.paymentMethods.model;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * POJO class for PaymentMethodRequest
 */
@Data
@NoArgsConstructor
public class PaymentMethodRequest {
  private String name;
  private String displayName;
  private PaymentType paymentType;
  List<PaymentPlanRequest> paymentPlans;

}
