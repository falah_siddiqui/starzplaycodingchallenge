package com.paymentMethods.model;

/**
 * enum for PaymentType
 */
public enum PaymentType {
  MOBILE_CARRIER, CREDIT_CARD, VOUCHER
}
