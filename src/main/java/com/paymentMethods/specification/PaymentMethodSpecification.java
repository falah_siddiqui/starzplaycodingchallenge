package com.paymentMethods.specification;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;
import com.paymentMethods.model.PaymentMethod;
import com.paymentMethods.model.PaymentMethodFilter;
import com.paymentMethods.model.PaymentPlan;

/**
 * This class defines jpa specifications using PaymentMethodFilter
 */
public class PaymentMethodSpecification {

  /**
   * No arguments constructor
   */
  private PaymentMethodSpecification() {}

  /**
   * This method finds all the records based on the specification
   *
   * @param paymentMethodFilter
   * @return
   */
  public static Specification<PaymentMethod> findAll(PaymentMethodFilter paymentMethodFilter) {

    return (Specification<PaymentMethod>) (root, cq, cb) -> {
      final Collection<Predicate> predicates = new ArrayList<>();

      Path<PaymentPlan> paymentPlanPath = root.join("paymentPlans", JoinType.LEFT);

      if (!ObjectUtils.isEmpty(paymentMethodFilter.getName())) {
        predicates.add(cb.like(root.get("name"), "%" + paymentMethodFilter.getName() + "%"));
      }

      if (!ObjectUtils.isEmpty(paymentMethodFilter.getId())) {
        predicates.add(cb.equal(paymentPlanPath.get("id"), paymentMethodFilter.getId()));
      }
      return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    };
  }
}
